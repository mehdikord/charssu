
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('test',[\App\Http\Controllers\Api\Manage\DashboardController::class,'shop_main']);
//helpers
Route::prefix('helpers')->group(function (){
    Route::get('get/provinces',[\App\Http\Controllers\Api\HelperController::class,'get_provinces']);
    Route::get('get/brands',[\App\Http\Controllers\Api\HelperController::class,'get_brands']);
    Route::get('get/problems',[\App\Http\Controllers\Api\HelperController::class,'get_problems']);
    Route::get('get/product-categories',[\App\Http\Controllers\Api\HelperController::class,'get_product_categories']);
    Route::get('install-zones',[\App\Http\Controllers\Api\HelperController::class,'install_zone']);
    Route::prefix('shop')->group(function (){
        Route::prefix('products')->group(function (){
            Route::get('',[\App\Http\Controllers\Api\Front\ShopController::class,'products']);
            Route::get('single/{code}',[\App\Http\Controllers\Api\Front\ShopController::class,'single_product']);
            Route::get('categories',[\App\Http\Controllers\Api\Front\ShopController::class,'categories']);
        });


    });

});



//management api
Route::group(['prefix'=>'management'],static function (){

    //Helpers
    Route::prefix('helpers')->group(function (){
        Route::get('get/provinces',[\App\Http\Controllers\Api\HelperController::class,'get_provinces']);
        Route::get('get/brands',[\App\Http\Controllers\Api\HelperController::class,'get_brands']);
    });

    Route::prefix('auth')->group(function (){
        Route::post('login',[\App\Http\Controllers\Api\Manage\AuthController::class,'login']);
        Route::post('login-check',[\App\Http\Controllers\Api\Manage\AuthController::class,'check']);

    });

    //set middleware
    Route::middleware('auth:users')->group(function (){

        //Dashboard
        Route::prefix('dashboard')->group(function (){
            Route::get('main-counts',[\App\Http\Controllers\Api\Manage\DashboardController::class,'main_counts']);
            Route::get('orders/waiting',[\App\Http\Controllers\Api\Manage\DashboardController::class,'waiting_orders']);
            Route::get('orders/running',[\App\Http\Controllers\Api\Manage\DashboardController::class,'running_orders']);
            Route::get('shop/main',[\App\Http\Controllers\Api\Manage\DashboardController::class,'shop_main']);
        });

        //Users
        Route::resource('users',\App\Http\Controllers\Api\Manage\UserController::class);
        Route::get('users/activation/{user}',[\App\Http\Controllers\Api\Manage\UserController::class,'activation']);

        //Customers
        Route::resource('customers',\App\Http\Controllers\Api\Manage\CustomerController::class);
        Route::get('customers/activation/{customer}',[\App\Http\Controllers\Api\Manage\CustomerController::class,'activation']);

        //Provinces
        Route::resource('zones',\App\Http\Controllers\Api\Manage\ZoneController::class);

        Route::prefix('servicemans')->group(function (){
            Route::get('waiting',[\App\Http\Controllers\Api\Manage\ServicemanController::class,'waiting']);
            Route::get('no-busy',[\App\Http\Controllers\Api\Manage\ServicemanController::class,'no_busy']);
            Route::get('set/accept/{serviceman}',[\App\Http\Controllers\Api\Manage\ServicemanController::class,'set_accept']);
            Route::post('set/reject/{serviceman}',[\App\Http\Controllers\Api\Manage\ServicemanController::class,'set_reject']);
        });

        Route::resource('servicemans',\App\Http\Controllers\Api\Manage\ServicemanController::class)->except('update','edit');
        Route::post('servicemans/{serviceman}',[\App\Http\Controllers\Api\Manage\ServicemanController::class,'update']);

        //device brands
        Route::resource('device-brands',\App\Http\Controllers\Api\Manage\DeviceBrandController::class);
        Route::post('device-brands/update/{brand}',[\App\Http\Controllers\Api\Manage\DeviceBrandController::class,'update']);

        //devices
        Route::resource('devices',\App\Http\Controllers\Api\Manage\DeviceController::class);
        Route::post('devices/update/{device}',[\App\Http\Controllers\Api\Manage\DeviceController::class,'update']);

        //problems
        Route::resource('problems',\App\Http\Controllers\Api\Manage\ProblemController::class);

        //products categories
        Route::resource('product-categories',\App\Http\Controllers\Api\Manage\ProductCategoryController::class);

        //products
        Route::resource('products',\App\Http\Controllers\Api\Manage\ProductController::class);

        Route::prefix('products')->group(function (){
            Route::get('images/{product}',[\App\Http\Controllers\Api\Manage\ProductController::class,'images']);
            Route::post('images/{product}',[\App\Http\Controllers\Api\Manage\ProductController::class,'images_store']);
            Route::delete('images/{image}',[\App\Http\Controllers\Api\Manage\ProductController::class,'images_delete']);

        });

        //costs
        Route::resource('costs',\App\Http\Controllers\Api\Manage\CostController::class)->except('create','show','edit');
        Route::get('costs/activation/{cost}',[\App\Http\Controllers\Api\Manage\CostController::class,'activation']);

        //Orders
        Route::prefix('orders')->group(function (){
            Route::get('new',[\App\Http\Controllers\Api\Manage\OrderController::class,'new']);
            Route::get('running',[\App\Http\Controllers\Api\Manage\OrderController::class,'running']);
            Route::get('done',[\App\Http\Controllers\Api\Manage\OrderController::class,'done']);
            Route::prefix('{order}')->group(function (){
                Route::get('',[\App\Http\Controllers\Api\Manage\OrderController::class,'show']);
                Route::get('remove',[\App\Http\Controllers\Api\Manage\OrderController::class,'remove']);
                Route::post('set/serviceman',[\App\Http\Controllers\Api\Manage\OrderController::class,'set_serviceman']);
                Route::post('change/serviceman',[\App\Http\Controllers\Api\Manage\OrderController::class,'change_serviceman']);
                Route::get('set/invoice',[\App\Http\Controllers\Api\Manage\OrderController::class,'set_invoice']);
                Route::get('remove/invoice',[\App\Http\Controllers\Api\Manage\OrderController::class,'remove_invoice']);
            });
            Route::get('change/invoice/{invoice}',[\App\Http\Controllers\Api\Manage\OrderController::class,'change_invoice']);


        });

        //Shop Orders
        Route::prefix('shop')->group(function (){
            Route::prefix('orders')->group(function (){
                Route::get('new',[\App\Http\Controllers\Api\Manage\ShopOrderController::class,'new_orders']);
                Route::get('sent',[\App\Http\Controllers\Api\Manage\ShopOrderController::class,'sent_orders']);
                Route::post('send-order/{order}',[\App\Http\Controllers\Api\Manage\ShopOrderController::class,'send_order']);

            });

        });


    });




});

//Customer Panel API
Route::prefix('customer')->group(function (){

    Route::prefix('auth')->group(function (){
        Route::post('login',[\App\Http\Controllers\Api\Customer\AuthController::class,'login']);
        Route::post('login-check',[\App\Http\Controllers\Api\Customer\AuthController::class,'login_check']);
    });

    Route::middleware('api_customer_auth')->group(function (){

        Route::prefix('profile')->group(function (){
            Route::get('',[\App\Http\Controllers\Api\Customer\ProfileController::class,'get_profile']);
            Route::post('',[\App\Http\Controllers\Api\Customer\ProfileController::class,'update_profile']);
        });

        Route::prefix('orders')->group(function (){
            Route::post('new',[\App\Http\Controllers\Api\Customer\OrderController::class,'new']);
            Route::get('running',[\App\Http\Controllers\Api\Customer\OrderController::class,'running']);
            Route::get('done',[\App\Http\Controllers\Api\Customer\OrderController::class,'done']);
            Route::get('details/{code}',[\App\Http\Controllers\Api\Customer\OrderController::class,'details']);
        });

        Route::prefix('invoices')->group(function (){
            Route::get('',[\App\Http\Controllers\Api\Customer\InvoiceController::class,'all']);
            Route::get('/{invoice}',[\App\Http\Controllers\Api\Customer\InvoiceController::class,'show']);

            Route::prefix('payment')->group(function (){
                Route::post('start',[\App\Http\Controllers\Api\Customer\InvoiceController::class,'start_payment']);
            });
        });

        Route::prefix('shopping')->group(function (){
            Route::prefix('invoices')->group(function (){
                Route::get('',[\App\Http\Controllers\Api\Customer\ShopController::class,'invoices']);
                Route::get('/{invoice}',[\App\Http\Controllers\Api\Customer\ShopController::class,'invoices_show']);
                Route::get('/pay/{invoice}',[\App\Http\Controllers\Api\Customer\ShopController::class,'invoice_pay']);
            });
            Route::post('payment/start',[\App\Http\Controllers\Api\Customer\ShopController::class,'start_payment']);

        });


    });

    Route::prefix('invoices')->group(function (){
        Route::prefix('payment')->group(function (){
            Route::post('shop/callback',[\App\Http\Controllers\Api\Customer\InvoiceController::class,'callback_payment_shop']);
            Route::post('callback',[\App\Http\Controllers\Api\Customer\InvoiceController::class,'callback_payment']);
        });
    });


});

//Application api
Route::group(['prefix' => 'app'],static function(){

    //servicemans

    Route::prefix('serviceman')->group(function (){

        Route::post('auth',[\App\Http\Controllers\Api\App\Serviceman\AuthController::class,'auth']);
        Route::post('auth/check',[\App\Http\Controllers\Api\App\Serviceman\AuthController::class,'auth_check']);
        Route::middleware('api_serviceman_auth')->group(function (){

            Route::get('profile',[\App\Http\Controllers\Api\App\Serviceman\ProfileController::class,'get_profile']);
            Route::post('profile',[\App\Http\Controllers\Api\App\Serviceman\ProfileController::class,'update_profile']);
            Route::post('profile/image',[\App\Http\Controllers\Api\App\Serviceman\ProfileController::class,'update_image']);
            Route::post('profile/file',[\App\Http\Controllers\Api\App\Serviceman\ProfileController::class,'update_files']);
            Route::get('activation',[\App\Http\Controllers\Api\App\Serviceman\ProfileController::class,'activation']);
            Route::prefix('orders')->group(function (){
                Route::get('new',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'new']);
                Route::get('done',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'done']);
                Route::get('active',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'active']);
                Route::post('accept',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'accept']);
                Route::post('cancel',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'cancel']);
                Route::post('cancel-reason',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'cancel_reason']);
                Route::get('single/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'single']);
                Route::post('notes/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'set_notes']);
                Route::get('notes/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'get_notes']);
                Route::delete('notes/{note}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'delete_note']);
                Route::get('products/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'get_products']);
                Route::post('products/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'set_products']);
                Route::delete('products/{product}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'delete_product']);
                Route::get('make-invoice/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'make_invoice']);
                Route::get('get-invoice/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'get_invoice']);
                Route::get('delete-invoice/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'delete_invoice']);
                Route::get('set/done/{order}',[\App\Http\Controllers\Api\App\Serviceman\OrderController::class,'set_done']);
                //payments
                Route::post('payment/start/{invoice}',[\App\Http\Controllers\Api\App\Serviceman\PaymentController::class,'start']);
                Route::post('payment/callback/',[\App\Http\Controllers\Api\App\Serviceman\PaymentController::class,'callback'])->withoutMiddleware('api_serviceman_auth');
            });

        });





    });



});
