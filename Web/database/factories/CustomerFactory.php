<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model=Customer::class;
    public function definition()
    {
        return [
            'province_id' => 8,
            'city_id' => 100,
            'zone_id' => 100,
            'name' => $this->faker->name,
            'phone' => "0912".rand(10000000,99999999),
            'address' => 'آدرس کامل من برای سایت چارسو',
            'tel' => '12365489565',
            'code' => engine_random_code(),
        ];
    }
}
