<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_invoices', function (Blueprint $table) {
            $table->boolean('is_sent')->default(0)->after('address');
            $table->string('sent_by')->nullable()->after('is_sent');
            $table->string('sent_description')->nullable()->after('sent_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_invoices', function (Blueprint $table) {
            //
        });
    }
};
