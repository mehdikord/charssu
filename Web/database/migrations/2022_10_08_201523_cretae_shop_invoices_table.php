<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('serviceman_id')->nullable();
            $table->string('price')->nullable();
            $table->boolean('is_pay')->default(0);
            $table->timestamp('paid_at')->nullable();
            $table->string('ref_id')->nullable();
            $table->string('pay_id')->nullable();
            $table->string('method')->nullable();
            $table->string('gateway')->nullable();
            $table->string('card_number')->nullable();
            $table->string('idpay_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('serviceman_id')->references('id')->on('servicemans')->onUpdate('cascade')->onDelete('set null');





            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_invoices');
    }
};
