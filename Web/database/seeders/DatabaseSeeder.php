<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Device_Brand;
use App\Models\Problem;
use App\Models\Product_Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        Customer::factory(150)->create();
        for ($i=0;$i<6;$i++){
            $brand = Device_Brand::create([
                'name' => "برند شماره ".$i,
            ]);
            for ($j=0;$j<5;$j++){
                $brand->devices()->create([
                    'name' => 'دستگاه '.rand(1000,9999),
                    'device_code' => rand(10000000,99999999),
                    'code' => engine_random_code(),
                ]);
            }
        }
        for ($i=0;$i<10;$i++){

            Problem::create([
                'problem' => 'مشکل و خرابی پکیج شماره '.$i,
                'price' => 100000 * ($i+1),
            ]);
        }
        for ($i=0;$i<4;$i++){
            $p_categories = Product_Category::create([
                'name' => 'دسته بندی '.$i,
            ]);
            for ($j=0;$j<8;$j++){
                $p_categories->products()->create([
                    'name' => 'محصول شماره '.$i.'-'.$j,
                    'device_brand_id' => Device_Brand::inRandomOrder()->first()->id,
                    'product_code' => strtoupper(Str::random(4)).engine_random_code('',4),
                    'code' => engine_random_code(),
                    'price' => 130000 * ($j+1),
                    'serviceman_price' => 100000 * ($j+1),
                    'description' => 'Lorem ipsum dolor sit amet consectetur, adipiscing elit nam lacus. Auctor sapien habitant congue vehicula orci at bibendum feugiat dis, mus habitasse mauris tortor cras sodales eleifend gravida, semper eros quis venenatis primis inceptos mi fermentum. Cum ullamcorper purus pulvinar velit orci lobortis netus diam cubilia facilisis duis himenaeos, sem eu bibendum odio eros egestas vivamus non sollicitudin sociis ut tincidunt, quis hendrerit dignissim commodo fermentum montes arcu metus quisque in litora. Ultricies lacinia vel cursus potenti purus fames, blandit auctor nam maecenas lectus luctus, iaculis tempor faucibus nisl porta. Auctor potenti morbi varius ornare vulputate vehicula euismod sagittis, luctus fames hendrerit quam scelerisque rhoncus. Felis fringilla ligula eget consequat neque vitae parturient sodales, enim a placerat aliquet cras justo sociis nulla, primis platea egestas suscipit congue at netus. Vitae netus id vel euismod posuere accumsan nostra habitant dui, iaculis parturient aptent nibh faucibus a fermentum gravida massa, sociosqu sed torquent elementum fames cras magnis conubia. Volutpat aptent per pharetra gravida vivamus montes nascetur posuere, sodales curabitur cubilia aenean duis luctus molestie sapien fringilla, in class potenti mattis et quis proin.',
                    'quantity' => rand(30,450),

                ]);
            }
        }


    }
}
