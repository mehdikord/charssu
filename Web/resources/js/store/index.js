import {createStore} from "vuex";

const store = createStore({
    state(){
        return {
            Auth : {},
            AuthPhone:null,
            Cart:{
                items :[],
            }

        }
    },
    mutations : {
        AuthLogin(state,item){
            state.Auth = item
            localStorage.setItem('charssu_admin_token',item.token)
            localStorage.setItem('charssu_admin_user',JSON.stringify(item.user))
        },
        AuthLogout(state){

            state.Auth = {}
            localStorage.removeItem('charssu_admin_token')
            localStorage.removeItem('charssu_admin_user')
        },
        AuthPhoneSet(state,phone){
            state.AuthPhone = phone;
            localStorage.setItem('charssu_auth_phone',phone);
        },
        AuthPhoneRemove(state){
            state.AuthPhone = null;
            localStorage.removeItem('charssu_auth_phone');
        },
        AuthPhoneSync(state){
            if (localStorage.getItem('charssu_auth_phone')){
                state.AuthPhone = localStorage.getItem('charssu_auth_phone')
            }
        },
        AuthSync(state){
            if (localStorage.getItem('charssu_admin_token') && localStorage.getItem('charssu_admin_user')){
                var item;
                item = {
                    token : localStorage.getItem('charssu_admin_token'),
                    user : JSON.parse(localStorage.getItem('charssu_admin_user')),
                }
                state.Auth = item
            }
        },

        CartAddToCart(state,item){
            state.Cart.items.push(item);
            localStorage.setItem('charssu_cart',JSON.stringify(state.Cart))
        },
        CartRemoveFromCart(state,index){
            state.Cart.items.splice(index,1);
            localStorage.setItem('charssu_cart',JSON.stringify(state.Cart))
        },
        CartSetEmpty(state){
            state.Cart.items=[];
            localStorage.removeItem('charssu_cart');
        },
        CartGetFromStorage(state){
            if (localStorage.getItem('charssu_cart')){
                state.Cart = JSON.parse(localStorage.getItem('charssu_cart'));
            }else {

            }
        }
    },
    getters : {
        AuthCheck : state => {
            return !!state.Auth.token;
        },
        AuthUser : state => {
            return state.Auth.user;
        },
        AutToken : state => {
            return state.Auth.token;
        },
        AuthPhoneGet : state => {
            return state.AuthPhone
        },

        CartItemCount: (state) => state.Cart.items.length,
        CartItemGet: (state) => state.Cart,
        CartTotalPrice : (state) => {
            let total=0;
            state.Cart.items.forEach(item => {
                if (item.product.sale !== null){
                    total+=parseInt(item.product.sale) * item.quantity;
                }else{
                    total+=parseInt(item.product.price) * item.quantity;
                }
            })
            return total;
        },
        CartTotalProducts : (state) => {
            let total=0;
            state.Cart.items.forEach(item => {
                total+=parseInt(item.quantity);
            })
            return total;
        }

    },
    actions : {
        Auth_Phone_Set(state,phone){
            state.commit("AuthPhoneSet",phone)
        },
        Auth_Phone_Remove(state){
            state.commit("AuthPhoneRemove")
        },
        Auth_Login(state,item){
            state.commit('AuthLogin',item)
        },
        Auth_Logout(state){
            state.commit('AuthLogout')
        }

    }

});

export default store
