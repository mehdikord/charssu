<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Shop_Invoice;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ShopController extends Controller
{
    public function __construct()
    {
        if (env('APP_PAYMENT','TEST') === 'TEST'){
            $this->sandbox = true;
        }else{
            $this->sandbox=false;
        }
    }
    public function start_payment(Request $request)
    {
        $validation = Validator::make($request->all(),[
           'cart' => 'required',
            'name' => 'required',
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'zone_id' => 'required|numeric',
            'address' => 'required',
            'postal_code' => 'required|numeric',
        ]);
        if ($validation->fails()){
            return response()->json($validation->errors(),421);
        }
        //Update Customer info
        customer_auth_get()->update([
            'name' => $request->name,
            'province_id' => $request->province_id,
            'city_id' => $request->city_id,
            'zone_id' => $request->zone_id,
            'address' => $request->address,
        ]);
        $amount = 0;
        //create invoice
        $invoice = Shop_Invoice::create([
            'customer_id' => customer_auth_get()->id,
            'address' => $request->address,
            'postal_code' => $request->postal_code,
            'method' => 'online',
            'gateway' => 'IDPay'
        ]);
        //check cart
        foreach ($request->cart as $cart){
            if (isset($cart['product'])){
                $product = Product::find($cart['product']['id']);
                if (!empty($product)){
                    if (!empty($product->sale)){
                        $price = $product->sale;
                    }else{
                        $price=$product->price;
                    }
                    $quantity = null;
                    if (isset($cart['quantity'])){
                        $quantity = $cart['quantity'];
                        $price *= $cart['quantity'];
                    }
                    $amount+=$price;
                    $invoice->orders()->create([
                        'product_id' => $product->id,
                        'quantity' => $quantity,
                        'price' => $price,
                    ]);
                }
            }
        }

        $invoice->update(['price' => $amount,'code' => engine_random_code($invoice->id)]);

        //start_payment
        $bank_url = "https://api.idpay.ir/v1.1/payment";
        $header = [
            'Content-Type' => 'application/json',
            "X-API-KEY" => "52020244-1b5d-4ea2-bac2-e2d6936cb5cc",
            'X-SANDBOX' => $this->sandbox,
        ];
        $callback = "http://charssu.ir/api/customer/invoices/payment/shop/callback";
        if (env('IDPAY_CALLBACK') === 'yes'){
            $callback = "http://localhost:8000/api/customer/invoices/payment/shop/callback";
        }

        $data = [
            'order_id' =>$invoice->id,
            'amount' => $invoice->price,
            'name' => customer_auth_get()->name,
            'phone' => customer_auth_get()->phone,
            'description' => 'پرداخت فاکتور فروشگاه در چارسو',
            'callback' => $callback,
        ];
        $client = new Client();
        $response = $client->post( $bank_url,
            [
                'json' => $data,
                'headers' => $header,
                'http_errors' => false
            ]);
        $response = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
        //Upgrade invoice
        $invoice->update([
            'idpay_id' => $response->id
        ]);
        return response()->json($response);
    }

    public function invoices()
    {
        $result = Shop_Invoice::query();
        $result->where('customer_id',customer_auth_get()->id);
        $result->with('orders');
        return response()->json($result->get());
    }

    public function invoices_show(Shop_Invoice $invoice)
    {
        if ($invoice->customer_id !== customer_auth_get()->id){
            return response()->json(['error' => 'forbidden'],403);
        }
        $invoice->load('orders');
        $invoice->load('orders.product');
        return response()->json($invoice);
    }

    public function invoice_pay(Shop_Invoice $invoice)
    {
        if ($invoice->customer_id !== customer_auth_get()->id){
            return response()->json(['error' => 'forbidden'],403);
        }

        $bank_url = "https://api.idpay.ir/v1.1/payment";
        $header = [
            'Content-Type' => 'application/json',
            "X-API-KEY" => "52020244-1b5d-4ea2-bac2-e2d6936cb5cc",
            'X-SANDBOX' => $this->sandbox,
        ];
        $callback = "http://charssu.ir/api/customer/invoices/payment/shop/callback";
        if (env('IDPAY_CALLBACK') === 'yes'){
            $callback = "http://localhost:8000/api/customer/invoices/payment/shop/callback";
        }

        $data = [
            'order_id' =>$invoice->id,
            'amount' => $invoice->price,
            'name' => customer_auth_get()->name,
            'phone' => customer_auth_get()->phone,
            'description' => 'پرداخت فاکتور فروشگاه در چارسو',
            'callback' => $callback,
        ];
        $client = new Client();
        $response = $client->post( $bank_url,
            [
                'json' => $data,
                'headers' => $header,
                'http_errors' => false
            ]);
        $response = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
        //Upgrade invoice
        $invoice->update([
            'idpay_id' => $response->id
        ]);
        return response()->json($response);



    }

}
