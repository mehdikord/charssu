<?php

namespace App\Http\Controllers\Api\Manage;

use App\Http\Controllers\Controller;
use App\Models\Shop_Invoice;
use App\Models\Shop_Order;
use Illuminate\Http\Request;

class ShopOrderController extends Controller
{
    public function new_orders()
    {
        $data = Shop_Invoice::query();
        $data->where('is_sent',false);
        $data->with(['customer' => function($customer){
            $customer->select(['id','name','phone']);
        }]);
        $data->with(['orders.product' => function($product){
            $product->select(['id','name','code']);
        }]);

        return response()->json($data->get());

    }

    public function sent_orders()
    {
        $data = Shop_Invoice::query();
        $data->where('is_sent',true);
        $data->with(['customer' => function($customer){
            $customer->select(['id','name','phone']);
        }]);
        $data->with(['orders.product' => function($product){
            $product->select(['id','name','code']);
        }]);

        return response()->json($data->get());

    }

    public function send_order(Shop_Invoice $order,Request $request)
    {
        $order->update([
            'is_sent' => true,
            'sent_by' => $request->sent_by,
            'sent_description' => $request->sent_description,
        ]);
        return response()->json("سفارش با موفقیت در لیست ارسال قرار گرفت");

    }
}
