<?php

namespace App\Http\Controllers\Api\Manage;

use App\Http\Controllers\Controller;
use App\Models\Cost;
use App\Models\Order;
use App\Models\Order_Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    public function show(Order $order)
    {
        $order->load('servicemans');
        $order->load(['servicemans.serviceman'=> function ($serviceman){
            $serviceman->select(['id','name','phone','code']);
        }]);
        $order->load('customer');
        $order->load('province');
        $order->load('city');
        $order->load('zone');
        $order->load('device_brand');
        $order->load('device');
        $order->load('problem');
        $order->load('notes');
        $order->load(['notes.serviceman'=> function ($serviceman_note){
            $serviceman_note->select(['id','name']);
        }]);
        $order->load('products');
        $order->load(['products.products' => function($product){
            $product->select(['id','name','code','price']);
        }]);
        $order->load('invoices');
        $order->load('invoices.details');
        return response()->json($order);
    }

    public function new()
    {
        $data = Order::query();
        $data->where('is_done',false);
        $data->where('pending',true);
        $data->with(['customer' => function($customer){
            $customer->select('id','name','phone');
        }]);
        $data->with(['device_brand' => function($customer){
            $customer->select('id','name');
        }]);
        $data->with(['device' => function($customer){
            $customer->select('id','name');
        }]);
        $data->with(['problem' => function($customer){
            $customer->select('id','problem');
        }]);
        return response()->json($data->get());
    }

    public function running()
    {
        $data = Order::query();
        $data->where('is_done',false);
        $data->where('pending',false);
        $data->whereHas('servicemans');
        $data->with(['servicemans.serviceman' => function($serviceman){
            $serviceman->select('id','name','phone');
        }]);
        $data->with(['customer' => function($customer){
            $customer->select('id','name','phone');
        }]);
        return response()->json($data->get());
    }

    public function done()
    {
        $data = Order::query();
        $data->where('is_done',true);
        $data->with(['servicemans.serviceman' => function($serviceman){
            $serviceman->select('id','name','phone');
        }]);
        $data->with(['servicemans' => function ($servicemans){
            $servicemans->where('accepted',true)->get();
        }]);
        $data->with(['customer' => function($customer){
            $customer->select('id','name','phone');
        }]);
        $data->with(['device_brand' => function($customer){
            $customer->select('id','name');
        }]);
        $data->with(['device' => function($customer){
            $customer->select('id','name');
        }]);
        return response()->json($data->get());
    }

    public function set_serviceman(Order $order,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'serviceman'=>"required|exists:servicemans,id",
        ]);
        if ($validation->fails()){
            return response()->json($validation->errors(),421);
        }
        $order->servicemans()->create([
            'serviceman_id' => $request->serviceman,
            'accepted' => true,
            'accepted_at' => Carbon::now(),
        ]);
        $order->update([
            'start_at' => Carbon::now(),
            'pending' => false,
        ]);
        return response()->json("سرویس کار مورد نظر باموفقیت به سفارش تخصیص یافت");

    }

    public function remove(Order $order)
    {
        $order->servicemans()->delete();
        $order->notes()->delete();
        $order->products()->delete();
        $order->invoices()->delete();
        $order->delete();
        return response()->json("سفارش مورد نظر باموفقیت حذف گردید");
    }

    public function change_serviceman(Order $order,Request $request)
    {
        $validation = Validator::make($request->all(),[
            'serviceman'=>"required|exists:servicemans,id",
        ]);
        if ($validation->fails()){
            return response()->json($validation->errors(),421);
        }
        $order->servicemans()->update(['accepted' => false,'canceled' => true,'canceled_at' => Carbon::now()]);
        $order->servicemans()->create([
            'serviceman_id' => $request->serviceman,
            'accepted' => true,
            'accepted_at' => Carbon::now(),
        ]);
        $order->update(['pending' => false]);
        return response()->json("سرویس کار سفارش مورد نظر باموفقیت تغییر یافت");
    }

    public function set_invoice(Order $order)
    {
        //check old invoice
        if ($order->invoice){
            return response()->json(['error' => 'فاکتور قبلا برا سفارش موردنظر ثبت شده است'],409);
        }

        //start making invoice for order
        $invoice = $order->invoices()->create([
            'type' => 'customer',
        ]);

        $total_price = 0;

        //make invoice details

        //set problem price
        if (!empty($order->problem_id)){
            $invoice->details()->create([
                'title' => $order->problem->problem,
                'price' => $order->problem->price,
            ]);
            $total_price += $order->problem->price;
        }

        //set costs
        foreach (Cost::where('is_active',true)->get() as $cost){
            $invoice->details()->create([
                'title' => $cost->title,
                'price' => $cost->price,
            ]);
            $total_price += $cost->price;
        }

        //set products (customers)
        foreach ($order->products()->where('paid','customer')->get() as $product){
            $product_price = 0;
            if (!empty($product->products->sale)){
                $product_price = $product->products->sale;
            }else{
                $product_price = $product->products->price;
            }
            $product_price *= $product->quantity;
            $product_title = $product->products->name."/کد: ".$product->products->code."/تعداد: ".$product->quantity;
            $invoice->details()->create([
                'title' => $product_title,
                'price' => $product_price,
                'is_product' => 1,
            ]);
            $total_price += $product_price;
        }

        $invoice->update([
            'price' => $total_price,
        ]);


        //check invoice for serviceman (products)
        if ($order->products()->where('paid','serviceman')->exists()){
            //start making invoice for serviceman
            $serviceman_invoice = $order->invoices()->create([
                'type' => 'serviceman',
            ]);

            $serviceman_total_price = 0;

            foreach ($order->products()->where('paid','serviceman')->get() as $serviceman_product){
                $serviceman_product_price = 0;
                if (!empty($serviceman_product->products->serviceman_price)){
                    $serviceman_product_price = $serviceman_product->products->serviceman_price;
                }else{
                    $serviceman_product_price = $serviceman_product->products->price;
                }
                $serviceman_product_price *= $serviceman_product->quantity;
                $product_title = $serviceman_product->products->name."/کد: ".$serviceman_product->products->code."/تعداد: ".$serviceman_product->quantity;
                $serviceman_invoice->details()->create([
                    'title' => $product_title,
                    'price' => $serviceman_product_price,
                    'is_product' => 1,
                ]);
                $serviceman_total_price += $serviceman_product_price;
            }

            $serviceman_invoice->update([
                'price' => $serviceman_total_price,
            ]);

        }

        //update order invoice bool
        $order->update(['invoice' => true]);
        return response()->json('فاکتور برای سفارش باموفقیت ثبت شد');

    }

    public function change_invoice(Order_Invoice $invoice)
    {
        if($invoice->is_pay){
          $invoice->update([
              'is_pay' =>false,
              'ref_id' => null,
              'paid_at' => null,
              'gateway' => null,
              'pay_id' => null,
              'card_number' => null,
              'idpay_id' => null,
          ]);
          $message = 'فاکتور به وضعیت پرداخت نشده تغییر یافت';
        }else{
            $invoice->update([
                'is_pay' =>true,
                'paid_at' => Carbon::now(),
                'gateway' => 'مدیریت',
            ]);
            $message = 'فاکتور به وضعیت پرداخت شده تغییر یافت';
        }
        return response()->json($message);

    }

    public function remove_invoice(Order $order)
    {
        $order->invoices()->delete();
        $order->update(['invoice' => false]);
        return response()->json("فاکتور های سفارش با موفقیت حذف شدند.");
    }
}
