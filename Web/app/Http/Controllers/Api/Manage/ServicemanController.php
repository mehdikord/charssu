<?php

namespace App\Http\Controllers\Api\Manage;

use App\Http\Controllers\Controller;
use App\Models\Serviceman;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ServicemanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Serviceman::with('user')
            ->where('is_accepted',true)
            ->with('province')
            ->with('city')
            ->with('zones')
            ->get()
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'profile'=>'nullable|image',
            'name' => 'required|min:3|max:225',
            'national_code'=>'required|numeric|unique:servicemans',
            'phone'=>'required|numeric|unique:servicemans',
            'birthday'=>"required",
            'address'=>"required",
            'start_time'=>"required",
            'end_time'=>"required",
        ]);

        if ($validation->fails()){
            return response()->json($validation->errors(),421);
        }
        $technical_license=null;
        $police_certificate=null;
        $non_addictions=null;
        $profile_url=null;
        if ($request->hasFile('technical_license')){
            $technical_license = Storage::put('private/servicemans/docs',$request->file('technical_license'));
        }
        if ($request->hasFile('police_certificate')){
            $police_certificate = Storage::put('private/servicemans/docs',$request->file('police_certificate'));
        }
        if ($request->hasFile('non_addictions')){
            $non_addictions = Storage::put('private/servicemans/docs',$request->file('non_addictions'));
        }
        if ($request->hasFile('profile')){
            $profile = Storage::put('public/servicemans/profile',$request->file('profile'));
            $profile_url = asset(Storage::url($profile));
        }

        //#TODO
        $result = Serviceman::create([
            'user_id'=> manage_auth_get()->id,
            'province_id'=>$request->province_id,
            'city_id'=>$request->city_id,
            'name'=>$request->name,
            'phone'=>$request->phone,
            'national_code'=>$request->national_code,
            'birthday'=>$request->birthday,
            'address'=>$request->address,
            'work_address'=>$request->work_address,
            'tel'=>$request->tel,
            'start_time'=>$request->start_time,
            'end_time'=>$request->end_time,
            'technical_license'=>$technical_license,
            'police_certificate'=>$police_certificate,
            'non_addictions'=>$non_addictions,
            'profile'=>$profile_url,
            'is_accepted' => 1,

        ]);
        //sync zones
        if ($request->filled('zones')){
            $zones = explode(',',$request->zones);
            $result->zones()->sync($zones);
        }
        //sync brands
        if ($request->filled('brands')){
            $brands = explode(',',$request->brands);
            $result->device_brands()->sync($brands);
        }
        //generate code
        $result->update(['code'=>engine_random_code('1'.$result->id)]);


        return response()->json('Create Successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Serviceman  $serviceman
     *
     */
    public function show(Serviceman $serviceman)
    {
        $serviceman->load('zones');
        $serviceman->load('device_brands');
        return response()->json($serviceman);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Serviceman  $serviceman
     * @return \Illuminate\Http\Response
     */
    public function edit(Serviceman $serviceman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Serviceman  $serviceman
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Serviceman $serviceman)
    {
        $validation = Validator::make($request->all(),[
            'province_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'profile'=>'nullable|image',
            'name' => 'required|min:3|max:225',
            "national_code'=>'required|numeric|unique:servicemans,national_code,$serviceman->id",
            'phone'=>"required|numeric|unique:servicemans,phone,$serviceman->id",
            'birthday'=>"required",
            'address'=>"required",
            'start_time'=>"required",
            'end_time'=>"required",
        ]);

        if ($validation->fails()){
            return response()->json($validation->errors(),421);
        }
        $technical_license=$serviceman->technical_license;
        $police_certificate=$serviceman->police_certificate;
        $non_addictions=$serviceman->non_addictions;
        $profile_url=$serviceman->profile;
        if ($request->hasFile('technical_license')){
            $technical_license = Storage::put('private/servicemans/docs',$request->file('technical_license'));
        }
        if ($request->hasFile('police_certificate')){
            $police_certificate = Storage::put('private/servicemans/docs',$request->file('police_certificate'));
        }
        if ($request->hasFile('non_addictions')){
            $non_addictions = Storage::put('private/servicemans/docs',$request->file('non_addictions'));
        }
        if ($request->hasFile('profile')){
            $profile = Storage::put('public/servicemans/profile',$request->file('profile'));
            $profile_url = asset(Storage::url($profile));
        }

        $serviceman->update([
            'province_id'=>$request->province_id,
            'city_id'=>$request->city_id,
            'name'=>$request->name,
            'phone'=>$request->phone,
            'national_code'=>$request->national_code,
            'birthday'=>$request->birthday,
            'address'=>$request->address,
            'work_address'=>$request->work_address,
            'tel'=>$request->tel,
            'start_time'=>$request->start_time,
            'end_time'=>$request->end_time,
            'technical_license'=>$technical_license,
            'police_certificate'=>$police_certificate,
            'non_addictions'=>$non_addictions,
            'profile'=>$profile_url,
        ]);
        //sync zones
        if ($request->filled('zones')){
            $zones = explode(',',$request->zones);
            $serviceman->zones()->sync($zones);
        }
        //sync brands
        if ($request->filled('brands')){
            $brands = explode(',',$request->brands);
            $serviceman->device_brands()->sync($brands);
        }

        return response()->json('Updated Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Serviceman  $serviceman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Serviceman $serviceman)
    {
        //
    }

    public function waiting()
    {
        return response()->json(Serviceman::where('is_accepted',false)
            ->where('is_rejected',false)
            ->with('user')
            ->with('province')
            ->with('city')
            ->with('zones')
            ->get()
        );

    }

    public function set_accept(Serviceman $serviceman)
    {
        $serviceman->update(['is_accepted' => true,'accepted_by'=> manage_auth_get()->id,'accepted_at'=>Carbon::now()]);
        return response()->json(['سرویس کار مورد نظر باموفقیت تایید شد و در حالت سفارش گیری قرار گرفت.']);

    }

    public function set_reject(Serviceman $serviceman,Request $request)
    {
        //send message
        $message = 'کاربر گرامی درخواست ثبت نام شما در چارسو به دلیل : " ';
        $message.=$request->message.' "';
        $message.='رد شد. لطفا اطاعات را کامل کرده و دوباره اقدام به ثبت نام نمایید.';
        $message.='\n';
        $message.='چارسو';
        helper_send_sms($serviceman->phone,$message);
        $serviceman->delete();
    }

    //all servicemans DoesntHave any active orders
    public function no_busy()
    {
        $result = Serviceman::query();
        $result->where('is_accepted',true)->where('is_active',true)->where('is_online',true);
        $servicemans=[];
        foreach ($result->get() as $item) {
            if (!api_check_serviceman_busy($item->id)) {
                $servicemans[]=$item;
            }
        }
        return $servicemans;
    }


}
