<?php

namespace App\Http\Controllers\Api\Manage;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Order_Product;
use App\Models\Product;
use App\Models\Serviceman;
use App\Models\Shop_Invoice;
use App\Models\Shop_Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function main_counts()
    {
        $customers = Customer::count();
        $servicemans = Serviceman::where('is_accepted',true)->count();
        $orders = Order::where('is_done',false)->count();
        $orders_all = Order::where('is_done',true)->count();
        return response()->json([
            'servicemans' => $servicemans,
            'orders' => $orders,
            'customers' => $customers,
            'orders_all' => $orders_all,
        ]);

    }

    public function waiting_orders()
    {
        $order = Order::query();
        $order->where('pending',true);
        $order->with(['customer' => function($customer){
            $customer->select(['id','name','phone']);
        }]);
        $order->with('city');
        $order->with('zone');
        $order->take(5);
        return response()->json($order->get());
    }

    public function running_orders()
    {
        $order = Order::query();
        $order->where('pending',false);
        $order->where('is_done',false);
        $order->with(['customer' => function($customer){
            $customer->select(['id','name','phone']);
        }]);
        $order->with('city');
        $order->with('zone');
        $order->take(5);
        return response()->json($order->get());
    }

    public function shop_main()
    {
        $product_count = Product::count();
        $sell_products = Order_Product::sum('quantity') + Shop_Order::sum('quantity');
        $sell_month = 0;
        $sell_total = 0;
        $sell_month+=Shop_Invoice::where('is_pay',true)->whereMonth('created_at',Carbon::now()->month)->sum('price');
        $sell_total+=Shop_Invoice::where('is_pay',true)->sum('price');
        foreach (Order_Product::whereMonth('created_at',Carbon::now()->month)->where('is_paid',true)->get() as $p_month){
            if ($p_month->paid == 'serviceman'){
                $sell_month+= $p_month->products->serviceman_price * $p_month->quantity;
            }else{
                $sell_month+= $p_month->products->price * $p_month->quantity;
            }
        }
        foreach (Order_Product::where('is_paid',true)->get() as $p_year){
            if ($p_year->paid == 'serviceman'){
                $sell_total+= $p_year->products->serviceman_price * $p_year->quantity;
            }else{
                $sell_total+= $p_year->products->price * $p_year->quantity;
            }
        }
        return response()->json([
            'product_count' => $product_count,
            'sell_products' => $sell_products,
            'sell_month' => $sell_month,
            'sell_total' => $sell_total,
        ]);

    }
}
