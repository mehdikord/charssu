<?php

namespace App\Http\Controllers\Api\App\Serviceman;

use App\Http\Controllers\Controller;
use App\Models\Order_Invoice;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public bool $sandbox;
    public function __construct()
    {
        if (env('APP_ENV','local') === 'local'){
            $this->sandbox = true;
        }else{
            $this->sandbox=false;
        }
    }

    public function start(Order_Invoice $invoice)
    {
        if ($invoice->is_pay){
            return response()->json(['error' => 'این فاکتور قبلا پرداخت شده است'],409);
        }
        if ($invoice->type !== 'serviceman'){
            return response()->json(['error' => 'این فاکتور باید توسط مشتری پرداخت شود'],409);
        }
        //send request for payment #TODO
        //Payment Data
        $bank_url = "https://api.idpay.ir/v1.1/payment";
        $header = [
            'Content-Type' => 'application/json',
            "X-API-KEY" => "52020244-1b5d-4ea2-bac2-e2d6936cb5cc",
            'X-SANDBOX' => $this->sandbox,
        ];
        $callback = "https://charssu.ir/api/app/serviceman/orders/payment/callback";
        if (env('IDPAY_CALLBACK') === 'yes'){
            $callback = "http://localhost:8000/api/app/serviceman/orders/payment/callback";
        }
        $data = [
            'order_id' =>$invoice->id,
            'amount' => $invoice->price,
            'name' => api_serviceman_get_user()->name,
            'phone' => api_serviceman_get_user()->phone,
            'description' => 'پرداخت فاکتور سفارش در چارسو',
            'callback' => $callback
        ];
        $client = new Client();
        $response = $client->post( $bank_url,
            [
                'json' => $data,
                'headers' => $header,
                'http_errors' => false
            ]);
        $response = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
        //Upgrade invoice
        $invoice->update([
            'idpay_id' => $response->id
        ]);
        return response()->json($response->link);

    }

    public function callback(Request $request)
    {

        $status = $request->status;
        $track_id = $request->track_id;
        $id = $request->id;
        $order_id = $request->order_id;
        $amount = $request->amount;
        $card_no = $request->card_no;
        $hashed_card_no = $request->hashed_card_no;
        $date = $request->date;
        $invoice = Order_Invoice::where('id',$order_id)->where('idpay_id',$id)->first();

        if (!empty($invoice)){
            $verify_url = "https://api.idpay.ir/v1.1/payment/verify";
            $header = [
                'Content-Type' => 'application/json',
                "X-API-KEY" => "52020244-1b5d-4ea2-bac2-e2d6936cb5cc",
                'X-SANDBOX' => $this->sandbox,
            ];
            $data = [
                'id' => $invoice->idpay_id,
                'order_id' => (string) $invoice->id,
            ];
            $client = new Client();
            $response = $client->post($verify_url,
                [
                    'json' => $data,
                    'headers' => $header,
                    'http_errors' => false
                ]);
            $response = json_decode($response->getBody(), false, 512, JSON_THROW_ON_ERROR);
            if (isset($response->status) && $response->status == 100){
                if (Order_Invoice::where('ref_id',$response->payment->track_id)->exists()){
                    return view('messages.app_serviceman_invoice_payment',['success' => false ,'error' => 'به دلیل تکراری بودن کد رهگیری بانکی ، پرداخت شما مورد تايید نمیباشد . ']);
                }
                //update invoice
                $invoice->update([
                    'ref_id' => $response->payment->track_id,
                    'pay_id' => $response->track_id,
                    'is_pay' => true,
                    'card_number' => $response->payment->card_no,
                    'paid_at' => Carbon::now(),
                    'gateway' => 'IDPay',
                    'method' => 'online'
                ]);
                return view('messages.app_serviceman_invoice_payment')->with(['success' => true]);
            }
            return view('messages.app_serviceman_invoice_payment',['success' => false ,'error' => 'تاییدیه پرداخت از سمت بانک امکان پذیر نیست']);
        }

        return view('messages.app_serviceman_invoice_payment',['success' => false ,'error' => 'تراکنش مورد نظر در سیستم یافت نشد !']);

    }


}
