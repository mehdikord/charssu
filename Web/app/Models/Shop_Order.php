<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop_Order extends Model
{
    use HasFactory;
    protected $table='shop_orders';
    protected $guarded=[];

    public function invoice()
    {
        return $this->belongsTo(Shop_Invoice::class,'shop_invoice_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
