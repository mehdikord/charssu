<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop_Invoice extends Model
{
    use HasFactory;
    protected $table='shop_invoices';
    protected $guarded=[ ];

    public function orders()
    {
        return $this->hasMany(Shop_Order::class,'shop_invoice_id');

    }

    public function customer()
    {
        return $this->belongsTo(Customer::class,'customer_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function serviceman()
    {
        return $this->belongsTo(Serviceman::class,'serviceman_id');
    }
}
